
# Ecom-WP


## Télécharger WordPress

(https://fr.wordpress.org/download/)

## Remplacer Wp-content

Une fois wordpress "extrait" et placer dans votre répertoire serveur :

- Remplacer le dossier "wp-content" par celui fourni.

## Importer la BDD

- Il vous faudra crée une BDD (le nom sera a renseigné lors de la procédure d'installation).
- Y importer le fichier **Ecom.sql**.

## Installer Wordpress

A la première ouverture de votre projet, suivez la procédure d'installation.

- **Attention** le préfixe doit être "wp_ecom_"

### Identifiant de Connection

- ID : Garry
- Pass : E@DL(i@QVirsJD5$yr